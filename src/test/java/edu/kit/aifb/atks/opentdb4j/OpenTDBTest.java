package edu.kit.aifb.atks.opentdb4j;

import edu.kit.aifb.atks.opentdb4j.OpenTDB;
import edu.kit.aifb.atks.opentdb4j.Question;
import edu.kit.aifb.atks.opentdb4j.QuestionType;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

class OpenTDBTest {

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void fetchQuestions() throws Exception {
        final List<Question> questions = OpenTDB.fetchQuestions(5, QuestionType.MULTIPLE_CHOICE);
        Assertions.assertEquals(5, questions.size());
        Assertions.assertNotEquals(0, questions.get(0).question().length());
        Assertions.assertNotEquals(0, questions.get(0).category().length());
        Assertions.assertNotEquals(0, questions.get(0).correctAnswer().length());
        Assertions.assertEquals(3, questions.get(0).incorrectAnswers().size());
        Assertions.assertEquals(QuestionType.MULTIPLE_CHOICE, questions.get(0).type());
    }
}