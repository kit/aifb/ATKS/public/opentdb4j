package edu.kit.aifb.atks.opentdb4j;

import java.util.List;

public record ApiResult(String responseCode, List<Question> results) {
}
