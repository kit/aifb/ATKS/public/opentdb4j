package edu.kit.aifb.atks.opentdb4j;

import com.google.gson.annotations.SerializedName;

/**
 * Enum representing trivia question difficulty
 */
public enum QuestionDifficulty {
    /**
     * Easy difficulty questions
     */
    @SerializedName("easy")
    EASY,

    /**
     * Medium difficulty questions
     */
    @SerializedName("medium")
    MEDIUM,

    /**
     * Hard difficulty questions
     */
    @SerializedName("hard")
    HARD
}
