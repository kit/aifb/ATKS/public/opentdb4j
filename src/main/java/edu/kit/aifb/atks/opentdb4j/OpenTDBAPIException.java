package edu.kit.aifb.atks.opentdb4j;

public class OpenTDBAPIException extends Exception {
    public OpenTDBAPIException(String message) {
        super(message);
    }

    public OpenTDBAPIException(String message, Throwable cause) {
        super(message, cause);
    }
}
