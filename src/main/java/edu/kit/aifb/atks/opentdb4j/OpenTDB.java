package edu.kit.aifb.atks.opentdb4j;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.FieldNamingStrategy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.List;

/**
 * A simple Java interface wrapper around <a href="https://opentdb.com/">Open Trivia Database</a>.
 */
public class OpenTDB {
    private static final String API_BASE_URL = "https://opentdb.com/api.php";

    static final HttpClient httpClient = HttpClient.newHttpClient();
    static final Gson gson = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES).create();

    /**
     * Fetch one question of arbitrary type, category, and difficulty.
     * @return Single-item list of quiz questions.
     * @throws OpenTDBAPIException if API or network fails.
     */
    public static List<Question> fetchQuestions() throws OpenTDBAPIException {
        return fetchQuestions(1);
    }

    /**
     * Fetch the specified number of questions of arbitrary type, category, and difficulty.
     * @param number How many questions to fetch
     * @return List of quiz questions.
     * @throws OpenTDBAPIException if API or network fails.
     */
    public static List<Question> fetchQuestions(int number) throws OpenTDBAPIException {
        return fetchQuestions(number, null);
    }

    /**
     * Fetch the specified number of questions of given type and arbitrary category, and difficulty.
     * @param number How many questions to fetch
     * @param type The type of question to query
     * @return List of quiz questions.
     * @throws OpenTDBAPIException if API or network fails.
     */
    public static List<Question> fetchQuestions(int number, QuestionType type) throws OpenTDBAPIException {
        return fetchQuestions(number, type, null, null);
    }

    /**
     * Fetch the specified number of questions of given type, category, and difficulty.
     * @param number How many questions to fetch
     * @param type The type of question to query
     * @param difficulty The difficulty of questions to fetch
     * @param category The category of questions to fetch
     * @return List of quiz questions.
     * @throws OpenTDBAPIException if API or network fails.
     */
    public static List<Question> fetchQuestions(int number, QuestionType type, QuestionDifficulty difficulty, String category) throws OpenTDBAPIException {
        final StringBuilder urlBuilder = new StringBuilder("%s?amount=%d".formatted(API_BASE_URL, number));
        if (type != null) urlBuilder.append("&type=%s".formatted(gson.toJson(type).replaceAll("\"", "")));
        if (difficulty != null) urlBuilder.append("&difficulty=%s".formatted(gson.toJson(difficulty).replaceAll("\"", "")));
        if (category != null) urlBuilder.append("&category=%s".formatted(category));

        final HttpRequest request = HttpRequest.newBuilder().uri(URI.create(urlBuilder.toString())).build();

        try {
            final HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
            if (response.statusCode() >= 400) {
                throw new OpenTDBAPIException("OpenTDB API returned error code %d".formatted(response.statusCode()));
            }
            return gson.fromJson(response.body(), ApiResult.class).results();
        } catch (IOException | InterruptedException e) {
            throw new OpenTDBAPIException("Failed to contact OpenTDB API, got error.", e);
        }
    }
}
