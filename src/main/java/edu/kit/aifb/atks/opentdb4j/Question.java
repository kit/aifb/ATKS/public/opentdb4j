package edu.kit.aifb.atks.opentdb4j;

import org.apache.commons.text.StringEscapeUtils;

import java.util.List;
import java.util.stream.Collectors;

/**
 * A trivia question object as returned by OpenTDB
 * @param question The actual question to display to the player
 * @param type The type of question
 * @param difficulty The question's difficulty
 * @param category The question's category
 * @param correctAnswer The correct answer in plain text
 * @param incorrectAnswers A list of incorrect answers in plain text
 */
public record Question(
        String question,
        QuestionType type,
        QuestionDifficulty difficulty,
        String category,
        String correctAnswer,
        List<String> incorrectAnswers
) {
    public Question(String question, QuestionType type, QuestionDifficulty difficulty, String category, String correctAnswer, List<String> incorrectAnswers) {
        this.question = StringEscapeUtils.unescapeHtml4(question);
        this.type = type;
        this.difficulty = difficulty;
        this.category = StringEscapeUtils.unescapeHtml4(category);
        this.correctAnswer = StringEscapeUtils.unescapeHtml4(correctAnswer);
        this.incorrectAnswers = incorrectAnswers.stream()
                .map(StringEscapeUtils::unescapeHtml4)
                .collect(Collectors.toList());
    }

    @Override
    public String toString() {
        return "Question: %s".formatted(this.question);
    }
}
