package edu.kit.aifb.atks.opentdb4j;

import com.google.gson.annotations.SerializedName;

/**
 * Enum representing the type of trivia question.
 */
public enum QuestionType {
    /**
     * Multiple (actually single-) choice questions
     */
    @SerializedName("multiple")
    MULTIPLE_CHOICE,

    /**
     * True / false questions
     */
    @SerializedName("boolean")
    TRUE_FALSE,
}
