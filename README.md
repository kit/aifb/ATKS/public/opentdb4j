# opentdb4j

A simple Java interface wrapper around [Open Trivia Database](https://opentdb.com) allowing to fetch quiz questions.

## Requirements
* Java >= 17

## API Documentation & Package Download

* JavaDoc available [here](https://docs.muetsch.io/opentdb4j/edu/kit/aifb/atks/opentdb4j/OpenTDB.html).
* Packaged fat-JAR can be downloaded [here](https://gitlab.kit.edu/kit/aifb/ATKS/public/opentdb4j/-/jobs/1146866/artifacts/file/target/opentdb4j-1.0.1.jar).

## Usage Example
```java
import edu.kit.aifb.atks.opentdb4j.*;

public static void main(String[] args) {
    try {
        final List<Question> questions = OpenTDB.fetchQuestions(8, QuestionType.MULTIPLE_CHOICE);
        questions.forEach(System.out::println);
    } catch (OpenTDBAPIException e) {
        System.err.println("Something went wrong :-/");
    }
}
```

### Output
```
Question: Which character does voice actress Tara Strong NOT voice?
Question: During the Roman Triumvirate of 42 BCE, what region of the Roman Republic was given to Lepidus?
Question: What is the stage name of English female rapper Mathangi Arulpragasam, who is known for the song "Paper Planes"?
Question: What is the smallest country in South America by area?
Question: What year was Queen Elizabeth II born?
Question: In "Highschool of the Dead", where did Komuro and Saeko establish to meet after the bus explosion?
Question: In "The Witness", how many lasers must be activated to get into the mountain area?
Question: From which album is the Gorillaz song, "On Melancholy Hill" featured in?
```

## Build

```bash
./mvnw clean package
```

## To Do

* [ ] CI to automatically build and deploy new JavaDocs

## License

MIT